package cat;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;

public class CAT {

    JFrame Cat = new JFrame("Start Page");
    private final JButton Sign = new JButton("Sign In");
    private final JButton Create = new JButton("Create Account");
    protected int Happy;
    protected int Hunger;
    protected int Level;
    protected int Money;
    protected int XP;
    protected String name;
    protected int test;

    public CAT() {
        Cat.setLocationRelativeTo(null);
        Cat.setLayout(new FlowLayout());
        Cat.add(Sign, FlowLayout.LEFT);
        Cat.add(Create, FlowLayout.LEFT);
        Sign.setFont(new java.awt.Font("Sitka Small", 1, 14));
        Create.setFont(new java.awt.Font("Sitka Small", 1, 14));

        Sign.addActionListener((ActionEvent arg0) -> {
            Start obj = new Start();
            obj.Start(0);
        });

        Create.addActionListener((ActionEvent arg0) -> {
            Start obj = new Start();
            obj.Start(1);
        });

        Cat.setSize(300, 100);
        Cat.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Cat.setVisible(true);
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(() -> {
            CAT cat = new CAT();
        });
    }
}

class Start extends CAT {

    JPanel panel = new JPanel();
    JButton btn = new JButton(" DONE ");

    public void Start(int num) {
        JFrame frame = new JFrame("ACCOUNT");
        frame.setLocationRelativeTo(null);
        frame.setLayout(new SpringLayout());
        frame.setSize(500, 200);

        JLabel label1 = new JLabel("Email : ");
        JTextField word1 = new JTextField(50);

        JLabel label2 = new JLabel("Password : ");
        JPasswordField word2 = new JPasswordField(50);

        JLabel label3 = new JLabel("Name of you cat : ");
        JTextField word3 = new JTextField(50);

        panel.setLayout(new GridBagLayout());
        GridBagConstraints bd = new GridBagConstraints();

        bd.gridx = 0;
        bd.gridy = 0;
        bd.anchor = GridBagConstraints.LINE_START;
        panel.add(label1, bd);

        bd.gridx = 1;
        bd.gridy = 0;
        bd.anchor = GridBagConstraints.LINE_END;
        panel.add(word1, bd);

        bd.gridx = 0;
        bd.gridy = 1;
        bd.anchor = GridBagConstraints.LINE_START;
        panel.add(label2, bd);

        bd.gridx = 1;
        bd.gridy = 1;
        bd.anchor = GridBagConstraints.LINE_END;
        panel.add(word2, bd);

        bd.gridx = 0;
        bd.gridy = 2;
        bd.anchor = GridBagConstraints.LINE_START;
        panel.add(label3, bd);

        bd.gridx = 1;
        bd.gridy = 2;
        bd.anchor = GridBagConstraints.LINE_END;
        panel.add(word3, bd);

        bd.gridx = 0;
        bd.gridy = 5;
        bd.anchor = GridBagConstraints.LAST_LINE_START;
        panel.add(btn, bd);
        frame.add(panel);

        btn.addActionListener((ActionEvent arg0) -> {
            String email = word1.getText();
            char[] c = word2.getPassword();
            String pass = new String(c);
            name = word3.getText();
            if (num == 1) {
                try {
                    PrintWriter input = new PrintWriter(new FileOutputStream("Data.txt", true));
                    PrintWriter out = new PrintWriter(new FileOutputStream("Gameplay.txt", true));
                    PrintWriter birth = new PrintWriter(new FileOutputStream("Birth.txt", true));
                    PrintWriter Stats = new PrintWriter(new FileOutputStream(name + ".txt"));
                    Scanner go = new Scanner(new FileInputStream("gameplay.txt"));
                    ArrayList<String> arr = new ArrayList<>();
                    while (go.hasNext()) {
                        String line = go.next();
                        arr.add(line);
                    }
                    go.close();

                    if (!arr.contains(name)) {
                        input.println(email + "\t" + pass);
                        out.println(name);
                        Stats.println("1 0 0 100 100 1000");
                        birth.println(name + " " + Time());
                        input.close();
                        out.close();
                        birth.close();
                        Stats.close();
                        JFrame simple = new JFrame();
                        simple.setLocationRelativeTo(null);
                        JTextArea text = new JTextArea("You have Created the account. Please Sign In");
                        text.setSize(100, 50);
                        simple.setLayout(new BorderLayout());
                        simple.setSize(300, 200);
                        simple.add(text, BorderLayout.CENTER);
                        simple.setVisible(true);
                    } else {
                        JFrame simple = new JFrame();
                        JTextArea text = new JTextArea("Your cat name is already taken.");
                        text.setSize(100, 50);
                        simple.setLocationRelativeTo(null);
                        simple.setLayout(new BorderLayout());
                        simple.setSize(300, 200);
                        simple.add(text, BorderLayout.CENTER);
                        simple.setVisible(true);
                    }
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            } else {
                try {
                    Scanner input = new Scanner(new FileInputStream("Data.txt"));
                    Scanner input2 = new Scanner(new FileInputStream(name + ".txt"));
                    ArrayList<String> arr = new ArrayList<>();
                    while (input.hasNext()) {
                        String line = input.next();
                        arr.add(line);
                    }

                    while (input2.hasNextInt()) {

                        Level = Integer.parseInt(input2.next());
                        Money = Integer.parseInt(input2.next());
                        XP = Integer.parseInt(input2.next());
                        Happy = Integer.parseInt(input2.next());
                        Hunger = Integer.parseInt(input2.next());
                        test = Integer.parseInt(input2.next());
                        break;

                    }

                    if (!arr.contains(email) || !arr.contains(pass)) {
                        JFrame frame1 = new JFrame();
                        JPanel panel1 = new JPanel();
                        frame1.setLocationRelativeTo(null);
                        frame1.setLayout(new FlowLayout());
                        frame1.setSize(300, 200);
                        JTextArea text = new JTextArea("The account does not exist. Create one");
                        text.setSize(100, 50);
                        panel1.add(text);
                        frame1.add(panel1);
                        frame1.setVisible(true);

                    } else {
                        new Game(Level, Money, XP, Happy, Hunger, test, name);
                    }

                    input.close();

                } catch (FileNotFoundException e) {
                    System.out.println(e.getMessage());
                }
            }
        });

        frame.setVisible(true);
    }

    public String Time() {

        LocalDateTime current = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        String DateTime = current.format(formatter);

        return DateTime;
    }
}

class Game extends CAT implements ActionListener, LineListener {

    boolean sound = false;
    private final JFrame frame = new JFrame("Cat Game");
    JProgressBar bar1;
    JProgressBar bar2;
    private final JPanel panel = new JPanel();
    private final JPanel panel2 = new JPanel();
    private final JPanel panel3 = new JPanel();
    private final JButton btn1 = new JButton("PLAY");
    private final JButton btn2 = new JButton("FEED");
    private final JButton btn3 = new JButton("CHAT");
    private final JButton btn4 = new JButton("SAVE");
    private final JButton btn5 = new JButton("SIGN OUT");
    private final ImageIcon image = new ImageIcon("tom1.png");
    private final JLabel label = new JLabel(image);
    protected JTextField word1 = new JTextField(" ", 10);
    JLabel label1 = new JLabel("Level : ");
    JTextField word = new JTextField(5);
    JLabel Expert = new JLabel("XP : ");
    JTextField amount = new JTextField(20);

    public Game(int a, int b, int c, int d, int h, int k, String s1) {

        Level = a;
        Money = b;
        XP = c;
        Happy = d;
        Hunger = h;
        test = k;
        name = s1;
        frame.setLocationRelativeTo(null);
        frame.setLayout(new BorderLayout());
        frame.add(panel, BorderLayout.SOUTH);
        panel.setLayout(new FlowLayout());
        panel.add(btn1, FlowLayout.LEFT);
        panel.add(btn2, FlowLayout.CENTER);
        panel.add(btn3, FlowLayout.RIGHT);

        frame.add(new JPanel(), BorderLayout.CENTER);
        frame.add(label, BorderLayout.CENTER);

        amount.setBackground(Color.CYAN);
        amount.setText(Integer.toString(XP) + "/" + Integer.toString(test));

        word.setBackground(Color.CYAN);
        word.setText(Integer.toString(Level));

        JLabel label = new JLabel("Munny : ");
        word1.setText(Integer.toString(Money));

        frame.add(panel2, BorderLayout.NORTH);
        panel2.setLayout(new FlowLayout());
        panel2.add(Expert, null);
        panel2.add(amount, null);
        panel2.add(label1, null);
        panel2.add(word, null);
        panel2.add(label, null);
        panel2.add(word1, null);
        panel2.add(btn4, null);
        panel2.add(btn5, null);

        UIManager.put("ProgressBar.selectionBackground", Color.RED);
        UIManager.put("ProgressBar.selectionForeground", Color.GREEN);
        bar1 = new JProgressBar();
        bar2 = new JProgressBar();
        bar1.setSize(200, 50);
        bar2.setSize(200, 50);
        bar1.setForeground(new Color(255, 255, 153));
        bar2.setForeground(new Color(255, 255, 153));
        bar1.setStringPainted(true);
        bar2.setStringPainted(true);
        JLabel Status = new JLabel("Stats : ");
        JLabel label2 = new JLabel("Happiness");
        JLabel label3 = new JLabel("Hunger");
        bar1.setValue(Happy);
        bar2.setValue(Hunger);

        frame.add(panel3, BorderLayout.WEST);
        panel3.setLayout(new GridBagLayout());
        GridBagConstraints bd = new GridBagConstraints();

        bd.gridx = 0;
        bd.gridy = 50;
        panel3.add(Status, bd);

        bd.gridx = 0;
        bd.gridy = 100;
        panel3.add(label2, bd);

        bd.gridx = 0;
        bd.gridy = 101;
        panel3.add(bar1, bd);

        bd.gridx = 0;
        bd.gridy = 400;
        panel3.add(label3, bd);

        bd.gridx = 0;
        bd.gridy = 401;
        panel3.add(bar2, bd);
        Timer timeDown = new Timer(60000, this);
        timeDown.start();

        btn1.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (Money < 0) {
                    Money = 0;
                } else if (Happy < 0) {
                    Happy = 0;
                } else if (Hunger < 0) {
                    Hunger = 0;
                } else if (Happy>=100){
                    Happy = 100;
                } else if (Hunger >= 100) {
                    Hunger = 100;
                } else{}
                
                Play p = new Play();
                JFrame f = new JFrame("PLay");
                f.setLocationRelativeTo(null);
                f.add(p);
                f.setSize(620, 630);
                f.setVisible(true);

                Money += 10;
                word1.setText(Integer.toString(Money));
                Happy += 10;
                Hunger -= 5;
                bar1.setValue(Happy);
                bar2.setValue(Hunger);
                XP += 3;
                setValue();
            }
        });

        btn2.addActionListener((ActionEvent e) -> {
            if (Money < 0) {
                Money = 0;
            } else if (Happy < 0) {
                Happy = 0;
            } else if (Hunger < 0) {
                Hunger = 0;
            } else if (Happy>=100){
                Happy = 100;
            } else if (Hunger >= 100) {
                Hunger = 100;
            } else{}
            
            if (Money < 10) {
                JFrame frame1 = new JFrame();
                JPanel panel1 = new JPanel();
                frame1.setLocationRelativeTo(null);
                frame1.setLayout(new FlowLayout());
                frame1.setSize(300, 200);
                JTextArea text = new JTextArea("You don't have enough munny. T_T ");
                text.setSize(100, 50);
                panel1.add(text);
                frame1.add(panel1);
                frame1.setVisible(true);
            } else {
                play();
                Happy += 10;
                Hunger += 10;
                Money -= 10;
                word1.setText(Integer.toString(Money));
                bar1.setValue(Happy);
                bar2.setValue(Hunger);
                XP += 3;
                setValue();
            }

        });

        btn3.addActionListener((ActionEvent e) -> {
            if (Happy < 0) {
                Happy = 0;
            } else if(Happy >= 100){
                Happy = 100;
            } else{}
            
            new Sound(name);
            Happy += 5;
            bar1.setValue(Happy);
            XP += 5;
            setValue();
        });

        btn4.addActionListener((ActionEvent e) -> {
            try {
                PrintWriter out = new PrintWriter(new FileOutputStream(name + ".txt"));
                out.println(Level + " " + Money + " " + XP + " " + Happy + " " + Hunger + " " + test);
                out.close();
            } catch (FileNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
        });

        btn5.addActionListener((ActionEvent e) -> {
            System.out.println("Thank you");
            System.exit(0);
        });
        frame.setSize(700, 700);
        frame.setVisible(true);
    }

    public void setValue() {

        if (XP > test) {
            XP = 0;
            amount.setText(Integer.toString(XP) + "/" + Integer.toString(test += 1000));
            word.setText(Integer.toString(Level += 1));
        } else {
            amount.setText(Integer.toString(XP) + "/" + Integer.toString(test));
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Happy -= 5;
        Hunger -= 5;

        if (Happy < 0 || Hunger < 0) {
            bar1.setValue(0);
            bar2.setValue(0);
        } else {
            bar1.setValue(Happy);
            bar2.setValue(Hunger);
        }
    }

    public void play() {

        sound = false;
        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(new File("Cat Meow.wav"));
            AudioFormat format = audioStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            Clip track = (Clip) AudioSystem.getLine(info);
            track.addLineListener(this);
            track.open(audioStream);
            track.start();

            while (!sound) {
                // wait for the playback completes
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

            track.close();

        } catch (UnsupportedAudioFileException ex) {
            System.out.println("The specified audio file is not supported.");
            ex.printStackTrace();
        } catch (LineUnavailableException ex) {
            System.out.println("Audio line for playing back is unavailable.");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error playing the audio file.");
            ex.printStackTrace();
        }

    }

    @Override
    public void update(LineEvent event) {
        LineEvent.Type type = event.getType();

        if (type == LineEvent.Type.START) {
        } else if (type == LineEvent.Type.STOP) {
            sound = true;
        }
    }
}

class Play extends JPanel implements ActionListener, KeyListener {

    Timer timer = new Timer(5, this);
    double x = 90, y = 570, velx = 0, vely = 0;
    int check = 0;
    Random myrand = new Random();
    int[] CoinX = new int[10];
    int[] CoinY = new int[10];
    boolean a = true, b = true, c = true, d = true, e = true, f = true, h = true, i = true, j = true, k = true;

    public void Rand() {

        for (int i = 0; i < 10; i++) {
            CoinX[i] = myrand.nextInt(570) + 30;
            CoinY[i] = myrand.nextInt(570) + 30;

            boolean p = (CoinX[i] > 40 && CoinX[i] < 140) && ((CoinY[i] > 40 && CoinY[i] < 140) || (CoinY[i] > 210 && CoinY[i] < 290) || (CoinY[i] > 350 && CoinY[i] < 590));
            boolean q = (CoinX[i] > 190 && CoinX[i] < 290) && ((CoinY[i] > 40 && CoinY[i] < 80) || (CoinY[i] > 160 && CoinY[i] < 200) || (CoinY[i] > 290 && CoinY[i] < 320) || (CoinY[i] > 400 && CoinY[i] < 470));
            boolean r = (CoinX[i] > 300 && CoinX[i] < 420) && ((CoinY[i] > 40 && CoinY[i] < 50) || (CoinY[i] > 100 && CoinY[i] < 170) || (CoinY[i] > 250 && CoinY[i] < 350) || (CoinX[i] > 370 && CoinX[i] < 410 && CoinY[i] > 490 && CoinY[i] < 560));
            boolean s = (CoinX[i] > 490 && CoinX[i] < 560 && CoinY[i] > 100 && CoinY[i] < 230) || (CoinX[i] > 490 && CoinX[i] < 560 && CoinY[i] > 340 && CoinY[i] < 410) || (CoinX[i] > 430 && CoinX[i] < 560 && CoinY[i] > 490 && CoinY[i] < 560);

            switch (check) {
                case 0:
                    if (!p) {
                        i--;
                    }
                    break;

                case 1:
                    if (!q) {
                        i--;
                    }
                    break;

                case 2:
                    if (!r) {
                        i--;
                    }
                    break;

                default:
                    if (!s) {
                        i--;
                    }
                    break;
            }

            for (int k = 0; k < i; k++) {
                if (CoinX[k] == CoinX[i] || CoinY[k] == CoinY[i]) {
                    i--;
                    break;
                }
            }

            if (check > 2) {
                check = 0;
            } else {
                check++;
            }
        }
    }

    public Play() {
        Rand();
        timer.start();
        setBackground(Color.CYAN);
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.YELLOW);
        g2.fill3DRect(CoinX[0], CoinY[0], 10, 30, a);
        g2.fill3DRect(CoinX[1], CoinY[1], 10, 30, b);
        g2.fill3DRect(CoinX[2], CoinY[2], 10, 30, c);
        g2.fill3DRect(CoinX[3], CoinY[3], 10, 30, d);
        g2.fill3DRect(CoinX[4], CoinY[4], 10, 30, e);
        g2.fill3DRect(CoinX[5], CoinY[5], 10, 30, f);
        g2.fill3DRect(CoinX[6], CoinY[6], 10, 30, h);
        g2.fill3DRect(CoinX[7], CoinY[7], 10, 30, i);
        g2.fill3DRect(CoinX[8], CoinY[8], 10, 30, j);
        g2.fill3DRect(CoinX[9], CoinY[9], 10, 30, k);

        g2.setColor(Color.BLACK);
        g2.fill(new Ellipse2D.Double(x, y, 20, 20));
        g2.setColor(Color.RED);
        g2.fill(new Rectangle2D.Double(0, 30, 30, 120));
        g2.fill(new Rectangle2D.Double(0, 180, 30, 120));
        g2.fill(new Rectangle2D.Double(0, 330, 30, 120));
        g2.fill(new Rectangle2D.Double(0, 480, 30, 120));
        g2.fill(new Rectangle2D.Double(90, 0, 180, 30));
        g2.fill(new Rectangle2D.Double(180, 90, 90, 60));
        g2.fill(new Rectangle2D.Double(120, 150, 60, 60));
        g2.fill(new Rectangle2D.Double(180, 210, 90, 60));
        g2.fill(new Rectangle2D.Double(120, 300, 60, 60));
        g2.fill(new Rectangle2D.Double(180, 330, 90, 60));
        g2.fill(new Rectangle2D.Double(300, 360, 60, 120));
        g2.fill(new Rectangle2D.Double(150, 390, 30, 90));
        g2.fill(new Rectangle2D.Double(180, 480, 180, 60));
        g2.fill(new Rectangle2D.Double(180, 540, 60, 60));
        g2.fill(new Rectangle2D.Double(330, 0, 90, 30));
        g2.fill(new Rectangle2D.Double(330, 60, 120, 30));
        g2.fill(new Rectangle2D.Double(420, 90, 30, 30));
        g2.fill(new Rectangle2D.Double(420, 150, 90, 30));
        g2.fill(new Rectangle2D.Double(330, 180, 90, 60));
        g2.fill(new Rectangle2D.Double(420, 240, 60, 180));
        g2.fill(new Rectangle2D.Double(360, 420, 60, 60));
        g2.fill(new Rectangle2D.Double(480, 420, 60, 60));
        g2.fill(new Rectangle2D.Double(300, 570, 90, 30));
        g2.fill(new Rectangle2D.Double(450, 0, 150, 30));
        g2.fill(new Rectangle2D.Double(510, 30, 30, 60));
        g2.fill(new Rectangle2D.Double(570, 30, 30, 90));
        g2.fill(new Rectangle2D.Double(570, 150, 30, 120));
        g2.fill(new Rectangle2D.Double(510, 240, 30, 90));
        g2.fill(new Rectangle2D.Double(570, 300, 30, 120));
        g2.fill(new Rectangle2D.Double(570, 450, 30, 150));
        g2.fill(new Rectangle2D.Double(450, 570, 120, 30));
    }

    @Override
    public void actionPerformed(ActionEvent ex) {

        check += timer.getDelay();

        if (check == 30000) {
            timer.stop();
        }

        if ((x >= 0 && x <= 30) && (y >= 10 && y <= 150)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 0 && x <= 30) && (y >= 160 && y <= 300)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 0 && x <= 30) && (y >= 310 && y <= 450)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 0 && x <= 30) && (y >= 460 && y <= 600)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 70 && x <= 270) && (y >= 0 && y <= 30)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 160 && x <= 270) && (y >= 70 && y <= 150)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 100 && x <= 180) && (y >= 130 && y <= 210)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 160 && x <= 270) && (y >= 190 && y <= 270)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 100 && x <= 180) && (y >= 280 && y <= 360)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 160 && x <= 270) && (y >= 310 && y <= 390)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 280 && x <= 360) && (y >= 340 && y <= 480)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 130 && x <= 180) && (y >= 370 && y <= 480)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 160 && x <= 360) && (y >= 460 && y <= 540)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 160 && x <= 240) && (y > 540 && y <= 600)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 310 && x <= 420) && (y >= 0 && y <= 30)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 310 && x <= 450) && (y >= 40 && y <= 90)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 400 && x <= 450) && (y > 90 && y <= 120)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 400 && x <= 510) && (y >= 130 && y <= 180)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 310 && x <= 420) && (y >= 160 && y <= 240)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 400 && x <= 480) && (y >= 220 && y <= 420)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 340 && x <= 420) && (y >= 400 && y <= 480)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 460 && x <= 540) && (y >= 400 && y <= 480)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 280 && x <= 390) && (y >= 550 && y <= 600)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 430 && x <= 600) && (y >= 0 && y <= 30)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 490 && x <= 540) && (y >= 10 && y <= 90)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 550 && x <= 600) && (y >= 10 && y <= 120)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 550 && x <= 600) && (y >= 130 && y <= 270)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 490 && x <= 540) && (y >= 220 && y <= 330)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 550 && x <= 600) && (y >= 280 && y <= 420)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 550 && x <= 600) && (y >= 430 && y <= 600)) {
            velx = -velx;
            vely = -vely;
        }

        if ((x >= 430 && x <= 570) && (y >= 550 && y <= 600)) {
            velx = -velx;
            vely = -vely;
        }

        if (x < 0 || x > 580) {
            velx = -velx;
        }

        if (y < 0 || y > 580) {
            vely = -vely;
        }

        x += velx;
        y += vely;

        if (!a && !b && !c && !d && !e && !f && !h && !i && !j && !k) {
            timer.stop();
        }

        if ((x > (CoinX[0] - 20) && x < (CoinX[0] + 10)) && (y > (CoinY[0] - 20) && y < (CoinY[0] + 30))) {
            CoinX[0] = -10;
            CoinY[0] = -10;
            a = false;
            repaint();
        } else if ((x > (CoinX[1] - 20) && x < (CoinX[1] + 10)) && (y > (CoinY[1] - 20) && y < (CoinY[1] + 30))) {
            CoinX[1] = -10;
            CoinY[1] = -10;
            b = false;
            repaint();
        } else if ((x > (CoinX[2] - 20) && x < (CoinX[2] + 10)) && (y > (CoinY[2] - 20) && y < (CoinY[2] + 30))) {
            CoinX[2] = -10;
            CoinY[2] = -10;
            c = false;
            repaint();
        } else if ((x > (CoinX[3] - 20) && x < (CoinX[3] + 10)) && (y > (CoinY[3] - 20) && y < (CoinY[3] + 30))) {
            CoinX[3] = -10;
            CoinY[3] = -10;
            d = false;
            repaint();
        } else if ((x > (CoinX[4] - 20) && x < (CoinX[4] + 10)) && (y > (CoinY[4] - 20) && y < (CoinY[4] + 30))) {
            CoinX[4] = -10;
            CoinY[4] = -10;
            e = false;
            repaint();
        } else if ((x > (CoinX[5] - 20) && x < (CoinX[5] + 10)) && (y > (CoinY[5] - 20) && y < (CoinY[5] + 30))) {
            CoinX[5] = -10;
            CoinY[5] = -10;
            f = false;
            repaint();
        } else if ((x > (CoinX[6] - 20) && x < (CoinX[6] + 10)) && (y > (CoinY[6] - 20) && y < (CoinY[6] + 30))) {
            CoinX[6] = -10;
            CoinY[6] = -10;
            h = false;
            repaint();
        } else if ((x > (CoinX[7] - 20) && x < (CoinX[7] + 10)) && (y > (CoinY[7] - 20) && y < (CoinY[7] + 30))) {
            CoinX[7] = -10;
            CoinY[7] = -10;
            i = false;
            repaint();
        } else if ((x > (CoinX[8] - 20) && x < (CoinX[8] + 10)) && (y > (CoinY[8] - 20) && y < (CoinY[8] + 30))) {
            CoinX[8] = -10;
            CoinY[8] = -10;
            j = false;
            repaint();
        } else if ((x > (CoinX[9] - 20) && x < (CoinX[9] + 10)) && (y > (CoinY[9] - 20) && y < (CoinY[9] + 30))) {
            CoinX[9] = -10;
            CoinY[9] = -10;
            k = false;
            repaint();
        } else {
            repaint();
        }
    }

    public void Up() {
        velx = 0;
        vely = -3;
    }

    public void Down() {
        velx = 0;
        vely = 3;
    }

    public void Right() {
        velx = 3;
        vely = 0;
    }

    public void Left() {
        velx = -3;
        vely = 0;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_UP) {
            Up();
        } else if (code == KeyEvent.VK_DOWN) {
            Down();
        } else if (code == KeyEvent.VK_RIGHT) {
            Right();
        } else if (code == KeyEvent.VK_LEFT) {
            Left();
        } else {
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}

class Sound extends Start implements LineListener {

    JFrame f = new JFrame("Lets Talk");
    JPanel p = new JPanel();
    JPanel p1 = new JPanel();
    JTextArea t = new JTextArea();
    JTextField in = new JTextField(50);
    JButton btn = new JButton("Send");
    String[] talk = {"Meow...", "Mmmmeeowwww", "Meowww Meowwww..", "Meoooww...", "Purrfff...", "Meow need attention"};
    String[] soundArr = {"Cat Meow.wav", "Cute Kitten.wav", "Double Meow.wav", "Normal Meow.wav", "Purr Sound.wav", "Sick Meow.wav"};
    boolean playComplete;

    public Sound(String s1) {

        name = s1;
        t.setBackground(Color.YELLOW);
        f.setLocationRelativeTo(null);
        f.setLayout(new BorderLayout());
        f.setSize(700, 500);
        f.add(t, BorderLayout.PAGE_START);
        f.add(new JScrollPane(t));

        f.add(p1, BorderLayout.PAGE_END);
        p1.setLayout(new FlowLayout());
        p1.add(in);
        p1.add(btn);

        btn.addActionListener((ActionEvent e) -> {
            Random myrand = new Random();
            int rand = myrand.nextInt(6);
            String word = in.getText();
            int x = 0;

            while (x < 2) {
                if (x == 0) {
                    t.append("User : " + word + "\n");
                    t.append(name + " : " + talk[rand] + "\n");
                } else {
                    play(rand);
                }
                x++;
            }
        });
        f.setVisible(true);
    }

    public void play(int num) {
        playComplete = false;

        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(new File(soundArr[num]));
            AudioFormat format = audioStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            Clip track = (Clip) AudioSystem.getLine(info);
            track.addLineListener(this);
            track.open(audioStream);
            track.start();

            while (!playComplete) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

            track.close();

        } catch (UnsupportedAudioFileException ex) {
            System.out.println("The specified audio file is not supported.");
            ex.printStackTrace();
        } catch (LineUnavailableException ex) {
            System.out.println("Audio line for playing back is unavailable.");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error playing the audio file.");
            ex.printStackTrace();
        }

    }

    @Override
    public void update(LineEvent event) {
        LineEvent.Type type = event.getType();

        if (type == LineEvent.Type.START) {
        } else if (type == LineEvent.Type.STOP) {
            playComplete = true;
        }
    }
}
